-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2021 at 03:35 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absensi_magang`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id_customer` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nama perusahaan` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `upload_logo` varchar(100) NOT NULL,
  `Status` enum('AKTIF','NON AKTIF') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id_customer`, `username`, `nama perusahaan`, `email`, `no_hp`, `password`, `alamat`, `upload_logo`, `Status`) VALUES
('1', 'ret', 'edae', 'gio@gmail.com', '76564534', '123', 'gf', '', 'AKTIF');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE `tbl_karyawan` (
  `id_karyawan` int(10) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `kode_bagian` varchar(255) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nomor_telepon` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('AKTIF','NONE') NOT NULL,
  `role_id` tinyint(1) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `id_customer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`id_karyawan`, `nama_karyawan`, `kode_bagian`, `alamat`, `nomor_telepon`, `email`, `password`, `status`, `role_id`, `foto`, `id_customer`) VALUES
(100, 'Gio Fandi', 'ACCOUNT MANAGER', 'bekasi', '5440848048', 'admin@gmail.com', '123', 'AKTIF', 3, '6f75fc737d4046fcec30fc16915cb4c0.png', ''),
(123, 'gol', 'FINANCE', 'efs', '55', 'gioandestum1@gmail.com', '123', 'AKTIF', 3, '0b75d26524c29d0488911f0ef6837397.jpg', ''),
(221, 'admin', '1', 'Kamboja ', '0898765', 'kristianmikhael@gmail.com', '123', 'AKTIF', 2, '7d9f924f58b70ac5361011298751c016.gif', ''),
(301, 'Fendrianto Hie', 'CEO', 'Tanggerang', '089668997301', 'fendriantohie@gmail.com', '123', 'AKTIF', 3, 'cf8b092215dffa21b9e592ad36f2c2f2.gif', ''),
(302, 'Salni Dewi', 'SALES MANAGER', 'Depok', '089668997302', 'salnidewi@gmail.com', '123', 'AKTIF', 3, '8f42846c8d6efa039ce411403256ecb4.jpg', ''),
(304, 'Djaja Wiguna', 'GENERAL MANAGER', 'Bekasi', '089668997304', 'djajawiguna@gmail.com', '123', 'AKTIF', 3, 'd6feef95ccd580833a8628a63bae43a8.jpg', ''),
(312, 'kala', 'ACCOUNTING', 'rwesr', '527', 'giandestum1@gmail.com', '123', 'AKTIF', 3, 'f8de1bf051c8ce21fce8ceb0396acc83.jpg', ''),
(325, 'Daniella Bolang', 'SECRETARY/HRD', 'Jakarta', '089668997325', 'daniellabolang@gmail.com', '123', 'AKTIF', 3, '04cd8634d59c0e7d91bb5c4e1cad9e80.png', ''),
(654, 'glole', 'ACCOUNTING', 'eqad', '55', 'gionainggolan1@gmail.com', '123', 'AKTIF', 3, '2e4262a210b424a76db94114d18bca53.jpg', ''),
(789, 'gol', 'CEO', 'jmj', '324', 'giandestum1@gmail.com', '123', 'AKTIF', 3, '418ca4e7076a5000ddc70c77d21ced47.png', ''),
(987, 'mikhael', 'CEO', 'ada', '324', 'gioandestum1@gmail.com', '123', 'AKTIF', 3, '81ce149b0cd374e8a2c3436ba6ffd20d.jpg', ''),
(2020, 'Manajemen', '64', 'Curug', '0898765', 'magangsemester6@gmail.com', '123', 'AKTIF', 1, '', ''),
(8585, 'iuo', 'CEO', 'kl', '225', 'giandestum1@gmail.com', '123', 'AKTIF', 3, 'd34d54d6df3674019562c556df49df73.jpg', ''),
(74654, 'gdg', 'ACCOUNTING', 'gfdg', '3432', 'admin@giofandigroup.com', '123', 'AKTIF', 3, '83993b2e0030b58859c7447da3f62700.jpg', ''),
(76534, 'golg', 'CEO', 'wqrdqw', '588', 'gionainggolan1@gmail.com', '123', 'AKTIF', 3, '23610725ca5792ca082a3c9e5d70c838.jpg', '12'),
(98756, 'gol', 'ACCOUNTING', 'rewr', '423', 'giandestum1@gmail.com', '123', 'AKTIF', 3, '7798d98c145f3dbb7b655b81185c671a.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perusahaan`
--

CREATE TABLE `tbl_perusahaan` (
  `id_perusahaan` varchar(100) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `id_customer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` varchar(110) NOT NULL,
  `id_customer` varchar(110) NOT NULL,
  `username` varchar(110) NOT NULL,
  `nama_perusahaan` varchar(110) NOT NULL,
  `alamat` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `upload_logo` varchar(110) NOT NULL,
  `Status` enum('AKTIF','NON AKTIF') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `id_customer`, `username`, `nama_perusahaan`, `alamat`, `email`, `password`, `upload_logo`, `Status`) VALUES
('1', '1', 'fd', 'fasda', 'fsad', 'gio@gmail.c', '123', '', 'AKTIF');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  ADD PRIMARY KEY (`id_karyawan`),
  ADD KEY `kode_bagian` (`kode_bagian`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  MODIFY `id_karyawan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=786665;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
