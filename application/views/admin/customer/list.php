<div id="content-wrapper">

      <div class="container-fluid">

      
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
            <div class="add">
            <a href="#" class="btn btn-primary btn-sm" tabindex="-1" role="button" aria-disabled="true"><i class="fas fa-plus-circle"></i> Tambah </a>
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr class="text-center">
                    <th>No</th>
                    <th>Id Customer</th>
                    <th>Username</th>
                    <th>Nama Perusahaan </th>
                    <th>Email</th>
                    <th>No Hp</th>
                    <th>Password</th>
                    <th>Alamat</th>
                    <th>Foto</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                 
                 $no = 1;
                 $query = $this->db->query("SELECT * FROM tbl_customer");
                  
                  foreach ($query->result_array() as $row)
                  {       
                ?>
                  <tr class="text-center">
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $row['id_customer'] ; ?></td>
                    <td><?php echo $row['username'] ; ?></td>
                    <td><?php echo $row['nama perusahaan'] ; ?></td>
                    <td><?php echo $row['email'] ; ?></td>
                    <td><?php echo $row['no_hp'] ; ?></td>
                    <td><?php echo $row['password'] ; ?></td>
                    <td><?php echo $row['alamat'] ; ?></td>
                    <td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row['upload_logo'];?>"></td>
                    <td><?php echo $row['Status'] ; ?></td>
                    <td><a href="<?php echo base_url('hrd/dashboard_hrd/delete?id=') .$row['id_customer']; ?>" class="btn btn-outline-danger"> Hapus </a>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?=$row['id_customer']?>">Edit </button> 
                    <a href="<?php echo base_url('admin/user/list?id=') .$row['id_customer']; ?>" class="btn btn-outline-succses"> Lihat User List </a>
                      </td>
                     
                      </tr>
<!-- Modaledit -->
<div id="myModal<?= $row['id_customer'] ?>" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
        <h4 class="modal-title">Edit data <?php echo $row['username'] ; ?></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				
				</div>
				<!-- body modal -->
				<div class="modal-body">
        <form action="<?php echo base_url()?>hrd/dashboard_hrd/edit"  method="post" enctype="multipart/form-data">
                <?php	
                $id = $this->input->get('id');
               // $cek_query = $this->karyawan->check_employe($id);

              
                ?>
    
                
                    <div class="container"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Id Customer </label>
                            <div class="col-sm-8">
                                <input type="text"   class="form-control" name="id_customer" placeholder="ID Customer" value="<?php echo $row['id_customer'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Username </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $row['username'] ?>" required>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA PERUSAHAAN </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="nama perusahaan" required >
                                    <option <?php echo ($row['nama perusahaan'] == '1') ? "selected": "" ?> value="01"> PT </option>
                                    <option <?php echo ($row['nama perusahaan'] == '2') ? "selected": "" ?> value="02"> PT </option>
                                    <option <?php echo ($row['nama perusahaan'] == '3') ? "selected": "" ?> value="03"> PT</option>
                                </select>
                            </div>
                        </div>
           
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> Email</label>
                            <div class="form-group col-sm-8">	
                                <textarea name="alamat" class="form-control" cols="20" rows="4"><?php echo $row['email']  ?> </textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> NOMOR TELEPON </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="no_hp" placeholder="NOMOR TELEPON" value="<?php echo $row['no_hp'] ?>" required>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> PASSWORD </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="password" placeholder="PASSWORD" value="<?php echo sha1($row['password']) ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> Alamat </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="alamat" placeholder="EMAIL" value="<?php echo $row['alamat'] ?>" required>
                            </div>
                        </div>
         
                        
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> FOTO </label>
                            <div class="form-group col-sm-8">	
                                    <input type="file" name="filefoto">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="control-label col-sm-3">Status</label>
                                <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="aktif" <?php if("aktif") { echo "SELECTED"; } ?>>Aktif</option>
                                    <option value="none" <?php if("none") { echo "SELECTED"; } ?>>Tidak Aktif</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10" style="float: right;">	
                                <button class="btn btn-lg btn-danger" name="batal">BATAL</button>
                                <button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
                            </div>	
                        </div>
                    </div>
               
                </form>
				</div>
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup </button>
				</div>
			</div>
		</div>
	</div>
  <!-- end modal edit -->
                  <?php } ?>
              


                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>