<div id="content-wrapper">

      <div class="container-fluid">

      
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
            <div class="back">
            <a href="<?= base_url().'admin/dashboard'?>" class="btn btn-info btn-sm" tabindex="-1" role="button" aria-disabled="true"><i class="fas fa-arrow-alt-circle-left"></i> Kembali ke Customer</a>
            </div>
            <br>
            <div class="add">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add User</button>
            <div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
                <h4 class="modal-title">Add User</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				
				</div>
				<!-- body modal -->
				<div class="modal-body">
                <form action="<?php echo base_url()?>admin/user/add"  method="post" enctype="multipart/form-data">
                    <div class="container"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> ID USER</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="id_karyawan" placeholder="ID User" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> ID Customer</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="id_customer" placeholder="ID Customer" required>
                            </div>
                        </div>
                      
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> USERNAME </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="username" placeholder="USERNAME" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA PERUSAHAAN </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="nama_perusahaan" required >
                                    <option> -- Pilih PERUSAHAAN -- </option>
                                    <option value="CEO"> PT </option>
                                    <option value="ACCOUNTING"> PT. </option>
                                    <option value="FINANCE"> PT. </option>
                                    <option value="SALES ADMIN"> PT. </option>
                                    <option value="ADMIN"> PT. </option>
                                
                                    <option value="SALES"> PT. </option>
                                </select>
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> ALAMAT </label>
                            <div class="form-group col-sm-8">	
                                <textarea name="alamat" class="form-control" cols="20" rows="4"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> EMAIL </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="email" placeholder="EMAIL" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> PASSWORD </label>
                            <div class="form-group col-sm-8">	
                                <input type="password" class="form-control" name="password" placeholder="PASSWORD" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> UPLOAD LOGO </label>
                            <div class="form-group col-sm-8">	
                                    <input type="file" name="filefoto">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="control-label col-sm-3">Status</label>
                                <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="aktif" <?php if("aktif") { echo "SELECTED"; } ?>>Aktif</option>
                                    <option value="none" <?php if("none") { echo "SELECTED"; } ?>>Tidak Aktif</option>
                                </select>
                                </div>
                        </div>

                       
                        <div class="form-group row">
                            <div class="col-sm-10" style="float: right;">	
                                <button class="btn btn-lg btn-danger" name="batal">BATAL</button>
                                <button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
                            </div>	
                        </div>
                    </div>
                </form>
			
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup </button>
				</div>
			</div>
		</div>
	</div>
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr class="text-center">
                    <th>No</th>
                    <th>Id User</th>
                    <th>Id Customer</th>
                    <th>Username</th>
                    <th>Nama Perusahaan </th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Foto</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                 
                 $no = 1;
            
                  
                  foreach ($query->result_array() as $row)
                  {       
                ?>
                  <tr class="text-center">
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $row['id_user'] ; ?></td>
                    <td><?php echo $row['id_customer'] ; ?></td>
                    <td><?php echo $row['username'] ; ?></td>
                    <td><?php echo $row['nama_perusahaan'] ; ?></td>
                    <td><?php echo $row['alamat'] ; ?></td>
                    <td><?php echo $row['email'] ; ?></td>
                    <td><?php echo $row['password'] ; ?></td>
                    <td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row['upload_logo'];?>"></td>
                    <td><?php echo $row['Status'] ; ?></td>
                    <td><a href="<?php echo base_url('hrd/dashboard_hrd/delete?id=') .$row['id_user']; ?>" class="btn btn-outline-danger"> Hapus </a>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?=$row['id_user']?>">Edit </button> 
                    
                      </td>
                     
                      </tr>
<!-- Modaledit -->
<div id="myModal<?= $row['id_user'] ?>" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
        <h4 class="modal-title">Edit data <?php echo $row['username'] ; ?></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				
				</div>
				<!-- body modal -->
				<div class="modal-body">
        <form action="<?php echo base_url()?>hrd/dashboard_hrd/edit"  method="post" enctype="multipart/form-data">
                <?php	
                $id = $this->input->get('id');
               // $cek_query = $this->karyawan->check_employe($id);

              
                ?>
    
                
                    <div class="container"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Id Customer </label>
                            <div class="col-sm-8">
                                <input type="text"   class="form-control" name="id_user" placeholder="ID Customer" value="<?php echo $row['id_user'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Id Customer </label>
                            <div class="col-sm-8">
                                <input type="text"   class="form-control" name="id_customer" placeholder="ID Customer" value="<?php echo $row['id_customer'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> Username </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $row['username'] ?>" required>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA PERUSAHAAN </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="nama_perusahaan" required >
                                    <option <?php echo ($row['nama_perusahaan'] == '1') ? "selected": "" ?> value="01"> PT </option>
                                    <option <?php echo ($row['nama_perusahaan'] == '2') ? "selected": "" ?> value="02"> PT </option>
                                    <option <?php echo ($row['nama_perusahaan'] == '3') ? "selected": "" ?> value="03"> PT</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> Alamat </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="alamat" placeholder="EMAIL" value="<?php echo $row['alamat'] ?>" required>
                            </div>
                        </div>
           
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> Email</label>
                            <div class="form-group col-sm-8">	
                                <textarea name="alamat" class="form-control" cols="20" rows="4"><?php echo $row['email']  ?> </textarea>
                            </div>
                        </div>
                    
                       
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> PASSWORD </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="password" placeholder="PASSWORD" value="<?php echo sha1($row['password']) ?>" required>
                            </div>
                        </div>
         
                        
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> FOTO </label>
                            <div class="form-group col-sm-8">	
                                    <input type="file" name="filefoto">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="control-label col-sm-3">Status</label>
                                <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="aktif" <?php if("aktif") { echo "SELECTED"; } ?>>Aktif</option>
                                    <option value="none" <?php if("none") { echo "SELECTED"; } ?>>Tidak Aktif</option>
                                </select>
                                </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10" style="float: right;">	
                                <button class="btn btn-lg btn-danger" name="batal">BATAL</button>
                                <button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
                            </div>	
                        </div>
                    </div>
               
                </form>
				</div>
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup </button>
				</div>
			</div>
		</div>
	</div>
  <!-- end modal edit -->
                  <?php } ?>
              


                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>