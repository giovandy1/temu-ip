<div id="content-wrapper">
    <div class="container-fluid">   
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><b>Halaman Utama HRD</b></h1>
                
        </div>
        <div class="row">
        <!-- Area Chart Example-->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-bullhorn mr-2" aria-hidden="true"></i> HRD</div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-xl-4 col-sm-6 mb-3">
                                    <div class="card text-white bg-primary o-hidden h-100">
                                        <div class="card-body">
                                            <div class="card-body-icon">
                                                <i class=""></i>
                                            </div>
                                            <div class="mr-5">LIHAT CUSTOMER/USER</div>
                                        </div>
                                        <a class="card-footer text-dark clearfix small z-1" href="<?php echo base_url('hrd/dashboard_hrd') ?>">
                                            <span class="float-left"><b>Lihat Data</b></span>
                                            <span class="float-right">
                                                <i class="fas fa-angle-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 mb-3">
                                    <div class="card text-white bg-warning o-hidden h-100">
                                        <div class="card-body">
                                            <div class="card-body-icon">
                                                <i class=""></i>
                                            </div>
                                            <div class="mr-5">TAMBAH CUSTOMER</div>
                                        </div>
                                        <a class="card-footer text-dark clearfix small z-1" href="<?php echo base_url() ?>hrd/dashboard_hrd/add">
                                            <span class="float-left"><b>Tambah customer</b></span>
                                            <span class="float-right">
                                                <i class="fas fa-angle-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-sm-6 mb-3">
                                    <div class="card text-white bg-danger o-hidden h-100">
                                        <div class="card-body">
                                            <div class="card-body-icon">
                                                <i class=""></i>
                                            </div>
                                            <div class="mr-5">TAMBAH User</div>
                                        </div>
                                        <a class="card-footer text-dark clearfix small z-1" href="<?php echo base_url() ?>hrd/dashboard_hrd/add_user">
                                            <span class="float-left"><b>Tambah user</b></span>
                                            <span class="float-right">
                                                <i class="fas fa-angle-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                </div>

                           
                            

                        </div>
                    </div>
                    <div class="card-footer small text-muted"></div>
                </div>
                        <!-- Area Chart Example-->
            </div>
        </div>
    </div>
</div>