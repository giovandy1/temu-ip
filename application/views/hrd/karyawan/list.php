<div id="content-wrapper">

      <div class="container-fluid">

      
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr class="text-center">
                    <th>No</th>
                    <th>Id Karyawan</th>
                    <th>Nama Karyawan</th>
                    <th>Nama Perusahaan </th>
                    <th>Status</th>
                    <th>Alamat</th>
                    <th>Nomor Telepon</th>
                    <th>Email</th>
                    <th>Foto</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                 
                 $no = 1;
                  $cek_query=$this->karyawan->list(); 
                  
                  foreach ($cek_query->result_array() as $row)
                  {       
                ?>
                  <tr class="text-center">
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $row['id_karyawan'] ; ?></td>
                    <td><?php echo $row['nama_karyawan'] ; ?></td>
                    <td><?php echo $row['kode_bagian'] ; ?></td>
                    <td><?php echo $row['status'] ; ?></td>
                    <td><?php echo $row['alamat'] ; ?></td>
                    <td><?php echo $row['nomor_telepon'] ; ?></td>
                    <td><?php echo $row['email'] ; ?></td>
                    <td><img style="width: 100px;" src="<?php echo base_url().'assets/images/'.$row['foto'];?>"></td>
                    <td><a href="<?php echo base_url('hrd/dashboard_hrd/delete?id=') .$row['id_karyawan']; ?>" class="btn btn-outline-danger"> Hapus </a>
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal<?=$row['id_karyawan']?>">Edit </button> 
                      
                      
                      </td>
                     
                      </tr>
<!-- Modaledit -->
<div id="myModal<?= $row['id_karyawan'] ?>" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
        <h4 class="modal-title">Edit data <?php echo $row['nama_karyawan'] ; ?></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				
				</div>
				<!-- body modal -->
				<div class="modal-body">
        <form action="<?php echo base_url()?>hrd/dashboard_hrd/edit"  method="post" enctype="multipart/form-data">
                <?php	
                $id = $this->input->get('id');
                $cek_query = $this->karyawan->check_employe($id);

              
                ?>
    
                
                    <div class="container"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> ID KARYAWAN </label>
                            <div class="col-sm-8">
                                <input type="text"   class="form-control" name="id_karyawan" placeholder="ID KARYAWAN" value="<?php echo $row['id_karyawan'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_karyawan" placeholder="NAMA LENGKAP KARYAWAN" value="<?php echo $row['nama_karyawan'] ?>" required>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA PERUSAHAAN </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="kode_bagian" required >
                                    <option <?php echo ($row['kode_bagian'] == '1') ? "selected": "" ?> value="01"> PT </option>
                                    <option <?php echo ($row['kode_bagian'] == '2') ? "selected": "" ?> value="02"> PT </option>
                                    <option <?php echo ($row['kode_bagian'] == '3') ? "selected": "" ?> value="03"> PT</option>
                                </select>
                            </div>
                        </div>
           
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> ALAMAT </label>
                            <div class="form-group col-sm-8">	
                                <textarea name="alamat" class="form-control" cols="20" rows="4"><?php echo $row['alamat']  ?> </textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> NOMOR TELEPON </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="nomor_telepon" placeholder="NOMOR TELEPON" value="<?php echo $row['nomor_telepon'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> EMAIL </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="email" placeholder="EMAIL" value="<?php echo $row['email'] ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> PASSWORD </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="password" placeholder="PASSWORD" value="<?php echo sha1($row['password']) ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="control-label col-sm-3">Status</label>
                                <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="aktif" <?php if("aktif") { echo "SELECTED"; } ?>>Aktif</option>
                                    <option value="none" <?php if("none") { echo "SELECTED"; } ?>>Tidak Aktif</option>
                                </select>
                                </div>
                        </div>
                        
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> FOTO </label>
                            <div class="form-group col-sm-8">	
                                    <input type="file" name="filefoto">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10" style="float: right;">	
                                <button class="btn btn-lg btn-danger" name="batal">BATAL</button>
                                <button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
                            </div>	
                        </div>
                    </div>
               
                </form>
				</div>
				<!-- footer modal -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup </button>
				</div>
			</div>
		</div>
	</div>
  <!-- end modal edit -->
                  <?php } ?>
              


                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>