<div id="content-wrapper">
    <div class="container-fluid">

        <!-- Area Chart Example-->
        <div class="card mb-3">
            <div class="card-header">
            <i class="fas fa-users"></i>
            Input Data User</div>
            <div class="card-body">
                
                <form action="<?php echo base_url()?>hrd/dashboard_hrd/add_user"  method="post" enctype="multipart/form-data">
                    <div class="container"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> ID USER</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="id_karyawan" placeholder="ID User" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> ID Customer</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="id_customer" placeholder="ID Customer" required>
                            </div>
                        </div>
                      
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> USERNAME </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_karyawan" placeholder="USERNAME" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"> NAMA PERUSAHAAN </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="kode_bagian" required >
                                    <option> -- Pilih PERUSAHAAN -- </option>
                                    <option value="CEO"> PT </option>
                                    <option value="ACCOUNTING"> PT. </option>
                                    <option value="FINANCE"> PT. </option>
                                    <option value="SALES ADMIN"> PT. </option>
                                    <option value="ADMIN"> PT. </option>
                                
                                    <option value="SALES"> PT. </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> AKSES WEB </label>
                            <div class="form-group col-sm-6">	
                                    <input type="radio" name="role_id" value="3" > User <br>
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> ALAMAT </label>
                            <div class="form-group col-sm-8">	
                                <textarea name="alamat" class="form-control" cols="20" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> NOMOR TELEPON </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="nomor_telepon" placeholder="NOMOR TELEPON" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> EMAIL </label>
                            <div class="form-group col-sm-8">	
                                <input type="text" class="form-control" name="email" placeholder="EMAIL" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> PASSWORD </label>
                            <div class="form-group col-sm-8">	
                                <input type="password" class="form-control" name="password" placeholder="PASSWORD" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="control-label col-sm-3">Status</label>
                                <div class="col-sm-3">
                                <select name="status" class="form-control">
                                    <option value="aktif" <?php if("aktif") { echo "SELECTED"; } ?>>Aktif</option>
                                    <option value="none" <?php if("none") { echo "SELECTED"; } ?>>Tidak Aktif</option>
                                </select>
                                </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label"> UPLOAD LOGO </label>
                            <div class="form-group col-sm-8">	
                                    <input type="file" name="filefoto">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10" style="float: right;">	
                                <button class="btn btn-lg btn-danger" name="batal">BATAL</button>
                                <button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
                            </div>	
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
<i class="fas fa-angle-up"></i>
</a>


