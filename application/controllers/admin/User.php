<?php

class User extends CI_Controller{

	public function __construct(){
		parent::__construct();
	
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		if( ($this->session->userdata('id_karyawan') == null) && ($this->session->userdata('role_id') != 2) ){
			redirect('login/login');
		}
		
    }

    public function list()
	{
        $id   = $this->input->get('id');
        $query = $this->db->query("Select * From tbl_user Where id_customer=$id");
		$data = array('title' => 'Halaman Admin',
					  'content' => 'admin/user/list',
                      'query'   => $query
                     );
                     
		$this->load->view('tamplate_bootstrap_admin/wrapper', $data, FALSE);
	}

    

    

}

    ?>