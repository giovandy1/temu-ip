<?php

class Dashboard extends CI_Controller{

	public function __construct(){
		parent::__construct();
	
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		if( ($this->session->userdata('id_karyawan') == null) && ($this->session->userdata('role_id') != 2) ){
			redirect('login/login');
		}
		
    }

    public function index()
	{
		$data = array('title' => 'Halaman Admin',
					  'content' => 'admin/customer/list'
                     );
                     
		$this->load->view('tamplate_bootstrap_admin/wrapper', $data, FALSE);
	}

    

    

}

    ?>